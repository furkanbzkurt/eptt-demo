package com.furkanbozkurt.epttavmdemo.ui.viewpager

import android.os.Bundle
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.furkanbozkurt.epttavmdemo.R
import com.furkanbozkurt.epttavmdemo.databinding.FragmentViewPagerBinding
import com.furkanbozkurt.epttavmdemo.ui.adapters.ViewPagerAdapter
import com.furkanbozkurt.epttavmdemo.util.Constants.Companion.FAVORITE_MOVIES_PAGE_INDEX
import com.furkanbozkurt.epttavmdemo.util.Constants.Companion.MOVIE_SEARCH_PAGE_INDEX
import com.furkanbozkurt.epttavmdemo.util.SharedViewModel
import com.google.android.material.tabs.TabLayoutMediator
import dagger.android.support.AndroidSupportInjection


class ViewPagerFragment : Fragment() {

    private lateinit var binding: FragmentViewPagerBinding
    private var sharedViewModel: SharedViewModel? = null
    private lateinit var searchView: SearchView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        binding = FragmentViewPagerBinding.inflate(inflater, container, false)

        init()
        setHasOptionsMenu(true)

        return binding.root
    }

    private fun init() {
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)
        activity?.let {
            sharedViewModel = ViewModelProvider(it).get(SharedViewModel::class.java)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search_movie, menu)
        initSearchView(menu)
        initUI()
    }

    private fun initSearchView(menu: Menu) {
        val searchItem = menu.findItem(R.id.search_movie)
        searchView = searchItem.actionView as SearchView
        searchView.setIconifiedByDefault(true)
        searchView.queryHint = "Search"

        searchView.setOnSearchClickListener {
            binding.textViewToolbarTitle.visibility = GONE
            searchView.setPadding(20, 0, 0, 0)
        }

        searchView.setOnCloseListener {
            binding.textViewToolbarTitle.visibility = VISIBLE
            searchView.setPadding(0, 0, 0, 0)
            false
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                sharedViewModel?.searchQuery?.postValue(query)
                return false
            }
        })
    }

    private fun initUI() {
        binding.viewPager.adapter = ViewPagerAdapter(this)

        // Set the icon and text for each tab
        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            tab.setIcon(getTabIcon(position))
        }.attach()

        binding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when (position) {
                    0 -> {
                        binding.textViewToolbarTitle.text = getString(R.string.movie_results)
                        setSearchViewVisibility(true)
                    }
                    1 -> {
                        binding.textViewToolbarTitle.text = getString(R.string.favorites)
                        setSearchViewVisibility(false)
                    }
                }
            }
        })
    }

    private fun setSearchViewVisibility(hide: Boolean) {
        searchView.isVisible = hide
        if (!hide) {
            searchView.setQuery("", false)
            if (!searchView.isIconified) {
                searchView.isIconified = true
            }
        }
    }

    private fun getTabTitle(position: Int): String? {
        return when (position) {
            MOVIE_SEARCH_PAGE_INDEX -> getString(R.string.title_movie_search)
            FAVORITE_MOVIES_PAGE_INDEX -> getString(R.string.title_favorite_movies)
            else -> null
        }
    }

    private fun getTabIcon(position: Int): Int {
        return when (position) {
            MOVIE_SEARCH_PAGE_INDEX -> R.drawable.ic_tab_movie
            FAVORITE_MOVIES_PAGE_INDEX -> R.drawable.ic_tab_favorite
            else -> throw IndexOutOfBoundsException()
        }
    }

}

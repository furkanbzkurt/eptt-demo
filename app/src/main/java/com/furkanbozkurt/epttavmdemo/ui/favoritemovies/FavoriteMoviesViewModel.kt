package com.furkanbozkurt.epttavmdemo.ui.favoritemovies

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.furkanbozkurt.epttavmdemo.data.repository.MovieRepository
import com.furkanbozkurt.epttavmdemo.model.Movie
import com.furkanbozkurt.epttavmdemo.util.State
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
class FavoriteMoviesViewModel @Inject constructor(
    private val movieRepository: MovieRepository
) : ViewModel() {

    private val _favoriteMoviesLiveData = MutableLiveData<State<List<Movie>>>()
    val favoriteMoviesLiveData: LiveData<State<List<Movie>>>
        get() = _favoriteMoviesLiveData

    val removedFavoriteMovie = MutableLiveData<Movie>()

    init {
        getFavoriteMovies()
    }

    private fun getFavoriteMovies() {
        viewModelScope.launch {
            movieRepository.getFavoriteMovies().collect {
                _favoriteMoviesLiveData.postValue(it)
            }
        }
    }

    fun addFavoriteMovie(movie: Movie) {
        viewModelScope.launch {
            movieRepository.addFavoriteMovie(movie)
        }

    }

    fun removeFavoriteMovie(movie: Movie) {
        viewModelScope.launch {
            removedFavoriteMovie.postValue(movie)
            movieRepository.removeFavoriteMovie(movie)
        }
    }
}
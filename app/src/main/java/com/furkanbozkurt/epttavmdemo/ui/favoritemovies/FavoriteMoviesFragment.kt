package com.furkanbozkurt.epttavmdemo.ui.favoritemovies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.furkanbozkurt.epttavmdemo.databinding.FragmentFavoriteMoviesBinding
import com.furkanbozkurt.epttavmdemo.model.Movie
import com.furkanbozkurt.epttavmdemo.ui.adapters.MovieListAdapter
import com.furkanbozkurt.epttavmdemo.util.SharedViewModel
import com.furkanbozkurt.epttavmdemo.util.State
import com.furkanbozkurt.epttavmdemo.util.ViewModelProvidersFactory
import com.furkanbozkurt.epttavmdemo.util.showToast
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class FavoriteMoviesFragment : Fragment(), MovieListAdapter.MovieClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvidersFactory

    private lateinit var binding: FragmentFavoriteMoviesBinding
    private lateinit var viewModel: FavoriteMoviesViewModel
    private var sharedViewModel: SharedViewModel? = null

    private val mAdapter: MovieListAdapter by lazy {
        MovieListAdapter(
            movieClickListener = this
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        binding = FragmentFavoriteMoviesBinding.inflate(inflater, container, false)

        init()
        observeFavoriteMovieEvents()

        return binding.root
    }

    private fun init() {
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(FavoriteMoviesViewModel::class.java)
        binding.recyclerViewFavoriteMovies.adapter = mAdapter
        binding.noDataState = true
        activity?.let {
            sharedViewModel = ViewModelProvider(it).get(SharedViewModel::class.java)
        }
    }

    private fun observeFavoriteMovieEvents() {
        viewModel.favoriteMoviesLiveData.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is State.Success -> {
                    binding.noDataState = state.data.isEmpty()
                    mAdapter.submitList(state.data.toMutableList())
                }
                is State.Error -> {
                    binding.loadingState = false
                    showToast(state.message)
                }
            }
        })

        // Notify other fragments to favorite movie removed via shared view model
        viewModel.removedFavoriteMovie.observe(viewLifecycleOwner, Observer {
            it?.let {
                sharedViewModel?.removedFavoriteMovie?.postValue(it)
            }
        })
    }

    override fun onAddFavoriteClick(movie: Movie) {
        viewModel.addFavoriteMovie(movie)
    }

    override fun onDeleteFavoriteClick(movie: Movie) {
        viewModel.removeFavoriteMovie(movie)
    }

}

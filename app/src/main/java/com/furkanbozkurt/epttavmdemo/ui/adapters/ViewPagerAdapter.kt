package com.furkanbozkurt.epttavmdemo.ui.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.furkanbozkurt.epttavmdemo.ui.favoritemovies.FavoriteMoviesFragment
import com.furkanbozkurt.epttavmdemo.ui.searchmovie.MovieSearchFragment
import com.furkanbozkurt.epttavmdemo.util.Constants.Companion.FAVORITE_MOVIES_PAGE_INDEX
import com.furkanbozkurt.epttavmdemo.util.Constants.Companion.MOVIE_SEARCH_PAGE_INDEX

class ViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    /**
     * Mapping of the ViewPager page indexes to their respective Fragments
     */
    private val tabFragmentsCreators: Map<Int, () -> Fragment> = mapOf(
        MOVIE_SEARCH_PAGE_INDEX to { MovieSearchFragment() },
        FAVORITE_MOVIES_PAGE_INDEX to { FavoriteMoviesFragment() }
    )

    override fun getItemCount() = tabFragmentsCreators.size

    override fun createFragment(position: Int): Fragment {
        return tabFragmentsCreators[position]?.invoke() ?: throw IndexOutOfBoundsException()
    }
}
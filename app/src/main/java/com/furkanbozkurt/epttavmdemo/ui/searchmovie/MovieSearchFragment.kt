package com.furkanbozkurt.epttavmdemo.ui.searchmovie

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.furkanbozkurt.epttavmdemo.R
import com.furkanbozkurt.epttavmdemo.databinding.FragmentMovieSearchBinding
import com.furkanbozkurt.epttavmdemo.model.Movie
import com.furkanbozkurt.epttavmdemo.ui.adapters.MovieListAdapter
import com.furkanbozkurt.epttavmdemo.util.*
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
class MovieSearchFragment : Fragment(), MovieListAdapter.MovieClickListener {

    @Inject
    lateinit var appContext: Context

    @Inject
    lateinit var viewModelFactory: ViewModelProvidersFactory

    private lateinit var binding: FragmentMovieSearchBinding
    private lateinit var viewModel: MovieSearchViewModel
    private var sharedViewModel: SharedViewModel? = null

    private val mAdapter: MovieListAdapter by lazy {
        MovieListAdapter(
            movieClickListener = this
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        binding = FragmentMovieSearchBinding.inflate(inflater, container, false)

        init()
        observeMovieSearchEvents()
        handleNetworkChanges()

        return binding.root
    }

    private fun init() {
        viewModel = ViewModelProvider(this, viewModelFactory).get(MovieSearchViewModel::class.java)
        activity?.let {
            sharedViewModel = ViewModelProvider(it).get(SharedViewModel::class.java)
        }
        binding.noDataState = true
        binding.loadingState = false
        binding.recyclerViewMovieResults.adapter = mAdapter
    }

    private fun observeMovieSearchEvents() {
        viewModel.movieSearchResultsLiveData.observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                is State.Loading -> {
                    binding.noDataState = false
                    binding.loadingState = true
                }
                is State.Success -> {
                    binding.loadingState = false
                    mAdapter.submitList(state.data.toMutableList())
                    if (state.data.isEmpty()) {
                        binding.noDataState = true
                        showToast(getString(R.string.no_result))
                    }
                }
                is State.Error -> {
                    binding.loadingState = false
                    showToast(state.message)
                }
            }
        })

        // Getting search query from viewpager fragment (search view host) through shared view model
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            sharedViewModel?.searchQuery?.observe(viewLifecycleOwner, Observer {
                // query from movie service with view model
                viewModel.getMovieSearchResults(it)
            })
        }

        // Getting deleted favorite from favorite movies fragment through shared view model
        sharedViewModel?.removedFavoriteMovie?.observe(viewLifecycleOwner, Observer {
            it?.let {
                // Notify adapter favorite movie removed
                mAdapter.movieRemovedFromFavorites(it)
            }
        })
    }

    override fun onAddFavoriteClick(movie: Movie) {
        viewModel.addFavoriteMovie(movie)
    }

    override fun onDeleteFavoriteClick(movie: Movie) {
        viewModel.removeFavoriteMovie(movie)
    }

    private fun handleNetworkChanges() {
        NetworkUtils.getNetworkLiveData(appContext)
            .observe(viewLifecycleOwner, Observer { isConnected ->
                if (!isConnected) {
                    binding.textViewNetworkStatus.text = getString(R.string.title_not_connected)
                    binding.networkStatusLayout.apply {
                        show()
                        setBackgroundColor(appContext.getColorRes(R.color.colorStatusNotConnected))
                    }
                } else {
                    binding.textViewNetworkStatus.text = getString(R.string.title_connected)
                    binding.networkStatusLayout.apply {
                        setBackgroundColor(appContext.getColorRes(R.color.colorStatusConnected))

                        animate()
                            .alpha(1f)
                            .setStartDelay(Constants.ANIMATION_DURATION)
                            .setDuration(Constants.ANIMATION_DURATION)
                            .setListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator) {
                                    hide()
                                }
                            })
                    }
                }
            })
    }
}

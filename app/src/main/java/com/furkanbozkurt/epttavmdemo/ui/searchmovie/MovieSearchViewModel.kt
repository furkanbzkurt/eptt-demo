package com.furkanbozkurt.epttavmdemo.ui.searchmovie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.furkanbozkurt.epttavmdemo.data.repository.MovieRepository
import com.furkanbozkurt.epttavmdemo.model.Movie
import com.furkanbozkurt.epttavmdemo.util.State
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
class MovieSearchViewModel @Inject constructor(
    private val movieRepository: MovieRepository
) : ViewModel() {

    private val _movieSearchResultsLiveData = MutableLiveData<State<List<Movie>>>()
    val movieSearchResultsLiveData: LiveData<State<List<Movie>>>
        get() = _movieSearchResultsLiveData


    fun getMovieSearchResults(searchQuery: String) {
        viewModelScope.launch {
            if (searchQuery.isNotEmpty()) {
                movieRepository.getMovieList(searchQuery).collect {
                    _movieSearchResultsLiveData.postValue(it)
                }
            }
        }
    }

    fun addFavoriteMovie(movie: Movie) {
        viewModelScope.launch {
            movieRepository.addFavoriteMovie(movie)
        }
    }

    fun removeFavoriteMovie(movie: Movie) {
        viewModelScope.launch {
            movieRepository.removeFavoriteMovie(movie)
        }
    }

}
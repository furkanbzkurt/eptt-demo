/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.furkanbozkurt.epttavmdemo.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.furkanbozkurt.epttavmdemo.R
import com.furkanbozkurt.epttavmdemo.databinding.ListItemNovieBinding
import com.furkanbozkurt.epttavmdemo.model.Movie
import com.furkanbozkurt.epttavmdemo.util.Constants.Companion.BASE_URL_MOVIE_POSTER
import com.like.LikeButton
import com.like.OnLikeListener


class MovieListAdapter(private val movieClickListener: MovieClickListener) :
    ListAdapter<Movie, RecyclerView.ViewHolder>(MovieDiffCallback()) {

    interface MovieClickListener {
        fun onAddFavoriteClick(movie: Movie)
        fun onDeleteFavoriteClick(movie: Movie)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MovieViewHolder(
            ListItemNovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val city = getItem(position)
        (holder as MovieViewHolder).bind(city, movieClickListener)
    }

    fun movieRemovedFromFavorites(movie: Movie) {
        for (i: Int in 0 until currentList.size) {
            if (movie.id == currentList[i].id) {
                currentList[i].isFavorite = false
                submitList(currentList)
                notifyDataSetChanged()
            }
        }
    }

    class MovieViewHolder(
        private val binding: ListItemNovieBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: Movie,
            movieClickListener: MovieClickListener
        ) {
            binding.apply {
                movie = item
                executePendingBindings()
            }

            binding.imageViewMoviePoster.load("${BASE_URL_MOVIE_POSTER}${item.posterPath}") {
                placeholder(R.drawable.ic_photo)
                error(R.drawable.ic_photo)
            }

            binding.buttonFavorite.setOnLikeListener(object : OnLikeListener {
                override fun liked(likeButton: LikeButton) {
                    item.isFavorite = true
                    movieClickListener.onAddFavoriteClick(item)
                }

                override fun unLiked(likeButton: LikeButton) {
                    item.isFavorite = false
                    movieClickListener.onDeleteFavoriteClick(item)
                }
            })
        }
    }
}

private class MovieDiffCallback : DiffUtil.ItemCallback<Movie>() {

    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.isFavorite == newItem.isFavorite
    }
}
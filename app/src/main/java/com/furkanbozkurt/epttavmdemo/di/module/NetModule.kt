package com.furkanbozkurt.epttavmdemo.di.module

import com.furkanbozkurt.epttavmdemo.data.remote.MovieService
import com.furkanbozkurt.epttavmdemo.util.Constants
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class NetModule {

    @Provides
    @Singleton
    fun provideOkHttp(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val okHttpBuilder =
            OkHttpClient().newBuilder()
                .addInterceptor(interceptor)
                .addInterceptor { chain ->
                    val request = chain.request().newBuilder()
                    val originalHttpUrl = chain.request().url
                    val url =
                        originalHttpUrl.newBuilder()
                            .addQueryParameter("api_key", Constants.TMDB_API_KEY)
                            .build()
                    request.url(url)
                    return@addInterceptor chain.proceed(request.build())
                }
        return okHttpBuilder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): MovieService {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constants.BASE_URL)
            .client(okHttpClient)
            .build()
            .create(MovieService::class.java)
    }

}
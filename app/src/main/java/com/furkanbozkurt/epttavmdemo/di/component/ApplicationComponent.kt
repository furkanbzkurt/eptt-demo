package com.furkanbozkurt.epttavmdemo.di.component

import com.furkanbozkurt.epttavmdemo.MainApp
import com.furkanbozkurt.epttavmdemo.di.module.ActivityBuilder
import com.furkanbozkurt.epttavmdemo.di.module.AppModule
import com.furkanbozkurt.epttavmdemo.di.module.NetModule
import com.furkanbozkurt.epttavmdemo.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [(AndroidInjectionModule::class),
        (AndroidSupportInjectionModule::class),
        (AppModule::class),
        (ActivityBuilder::class),
        (NetModule::class),
        (ViewModelModule::class)]
)

interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(mainApp: MainApp): Builder

        fun appModule(appModule: AppModule): Builder

        fun build(): ApplicationComponent
    }

    fun inject(mainApp: MainApp)
}
package com.furkanbozkurt.epttavmdemo.di.module;


import com.furkanbozkurt.epttavmdemo.ui.favoritemovies.FavoriteMoviesFragment;
import com.furkanbozkurt.epttavmdemo.ui.searchmovie.MovieSearchFragment;
import com.furkanbozkurt.epttavmdemo.ui.viewpager.ViewPagerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract ViewPagerFragment contributeViewPagerFragment();

    @ContributesAndroidInjector
    abstract MovieSearchFragment contributeMovieSearchFragment();

    @ContributesAndroidInjector
    abstract FavoriteMoviesFragment contributeFavoriteMoviesFragment();

}

package com.furkanbozkurt.epttavmdemo.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.furkanbozkurt.epttavmdemo.di.ViewModelKey
import com.furkanbozkurt.epttavmdemo.ui.favoritemovies.FavoriteMoviesViewModel
import com.furkanbozkurt.epttavmdemo.ui.searchmovie.MovieSearchViewModel
import com.furkanbozkurt.epttavmdemo.util.ViewModelProvidersFactory

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi

//@Suppress("unused")
@ExperimentalCoroutinesApi
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelProvidersFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MovieSearchViewModel::class)
    abstract fun provideMovieSearchViewModel(movieSearchViewModel: MovieSearchViewModel?): ViewModel?

    @Binds
    @IntoMap
    @ViewModelKey(FavoriteMoviesViewModel::class)
    abstract fun provideFavoriteMoviesViewModel(favoriteMoviesViewModel: FavoriteMoviesViewModel?): ViewModel?

}

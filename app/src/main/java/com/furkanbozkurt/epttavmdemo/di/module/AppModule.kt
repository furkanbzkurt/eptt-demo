package com.furkanbozkurt.epttavmdemo.di.module

import android.app.Application
import android.content.Context
import com.furkanbozkurt.epttavmdemo.data.local.AppDatabase
import com.furkanbozkurt.epttavmdemo.data.local.dao.MovieDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideAppContext(): Context = app

    @Provides
    @Singleton
    fun provideAppDatabase(app: Application) = AppDatabase.getInstance(app)

    @Provides
    @Singleton
    fun provideMovieDao(appDb: AppDatabase): MovieDao = appDb.movieDao()

}
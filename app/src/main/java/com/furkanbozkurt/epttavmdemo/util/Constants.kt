package com.furkanbozkurt.epttavmdemo.util

class Constants {
    companion object {
        const val DATABASE_NAME = "eptt_demo_db"
        const val BASE_URL = "https://api.themoviedb.org/"
        const val BASE_URL_MOVIE_POSTER = "http://image.tmdb.org/t/p/w185//"
        const val TMDB_API_KEY = "c9dc8235ee6e152edb494e6b5b764ec5"
        const val ANIMATION_DURATION = 1000L
        const val MOVIE_SEARCH_PAGE_INDEX = 0
        const val FAVORITE_MOVIES_PAGE_INDEX = 1
    }
}
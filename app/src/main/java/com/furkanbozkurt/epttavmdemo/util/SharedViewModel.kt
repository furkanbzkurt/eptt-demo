package com.furkanbozkurt.epttavmdemo.util

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.furkanbozkurt.epttavmdemo.model.Movie

class SharedViewModel : ViewModel() {
    val searchQuery = MutableLiveData<String>("")
    val removedFavoriteMovie = MutableLiveData<Movie>()
}
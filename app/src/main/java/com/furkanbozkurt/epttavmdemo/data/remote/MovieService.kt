package com.furkanbozkurt.epttavmdemo.data.remote

import com.furkanbozkurt.epttavmdemo.model.MovieResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieService {

    @GET("/3/search/movie")
    suspend fun getMovieSearchResults(@Query("query") search: String): MovieResponse

}
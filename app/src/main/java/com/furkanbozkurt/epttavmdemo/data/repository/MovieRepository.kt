package com.furkanbozkurt.epttavmdemo.data.repository

import com.furkanbozkurt.epttavmdemo.data.local.dao.MovieDao
import com.furkanbozkurt.epttavmdemo.data.remote.MovieService
import com.furkanbozkurt.epttavmdemo.model.Movie
import com.furkanbozkurt.epttavmdemo.util.State
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@ExperimentalCoroutinesApi
class MovieRepository @Inject constructor(
    private val movieDao: MovieDao,
    private val movieService: MovieService
) {

    fun getMovieList(searchQuery: String): Flow<State<List<Movie>>> {
        return flow<State<List<Movie>>> {
            emit(State.loading())
            val movieResults = mutableListOf<Movie>()
            val response = movieService.getMovieSearchResults(searchQuery)
            for (movie in response.results) {
                movie.isFavorite = movieDao.isMovieFavorite(movie.id) > 0
                movieResults.add(movie)
            }
            emit(State.success(movieResults))
        }.flowOn(Dispatchers.IO)
            .catch { e ->
                emit(State.error("" + e.message))
            }
    }

    fun getFavoriteMovies(): Flow<State<List<Movie>>> {
        return flow<State<List<Movie>>> {
            emit(State.loading())
            emitAll(movieDao.getFavoriteMovies().map {
                State.success(it)
            })
        }.flowOn(Dispatchers.IO)
            .catch { e ->
                emit(State.error("" + e.message))
            }
    }

    suspend fun addFavoriteMovie(movie: Movie) {
        movieDao.insertFavoriteMovie(movie)
    }

    suspend fun removeFavoriteMovie(movie: Movie) {
        movieDao.removeFavoriteMovie(movie.id)
    }

}
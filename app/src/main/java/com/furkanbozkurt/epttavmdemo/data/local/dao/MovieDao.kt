package com.furkanbozkurt.epttavmdemo.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.furkanbozkurt.epttavmdemo.model.Movie
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDao {

    @Query("SELECT COUNT() FROM ${Movie.TABLE_NAME} WHERE id = :movieId")
    fun isMovieFavorite(movieId: Int): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFavoriteMovie(movie: Movie)

    @Query("SELECT * FROM ${Movie.TABLE_NAME}")
    fun getFavoriteMovies(): Flow<List<Movie>>

    @Query("delete from ${Movie.TABLE_NAME} WHERE id = :movieId")
    suspend fun removeFavoriteMovie(movieId: Int)
}
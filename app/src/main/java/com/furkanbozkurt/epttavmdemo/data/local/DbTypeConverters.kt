package com.furkanbozkurt.epttavmdemo.data.local

import androidx.room.TypeConverter
import com.furkanbozkurt.epttavmdemo.model.Movie
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object DbTypeConverters {
    @TypeConverter
    @JvmStatic
    fun stringToIntList(data: String?): List<Int>? {
        return data?.let {
            it.split(",").map {
                try {
                    it.toInt()
                } catch (ex: NumberFormatException) {
                    null
                }
            }
        }?.filterNotNull()
    }

    @TypeConverter
    @JvmStatic
    fun intListToString(ints: List<Int>?): String? {
        return ints?.joinToString(",")
    }

}